using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameState { MainMenu, Playing, Pause, Finished, Win, Lose }

public class GameManager : MonoBehaviour
{
    [Header("UI")]
    [SerializeField] GameObject menuScreen;
    [SerializeField] GameObject loseScreen;
    [SerializeField] GameObject winScreen;

    private static GameManager _instance;

    private GameState state = GameState.MainMenu;

    bool winSheep = false;
    bool winWolf = false;

    public static GameState State { get => _instance.state; set { _instance.state = value; _instance.OnStateChanged(value); } }

    void Awake()
    {
        _instance = this;
    }

    void OnDestroy()
    {
        _instance = null;
    }

    void OnStateChanged(GameState newState)
    {
        Debug.Log($"Current state: {newState}");
        switch (newState)
        {
            case GameState.Win: winScreen.SetActive(true); break;
            case GameState.Lose: loseScreen.SetActive(true); break;
        }
    }

    public static void WinSheep()
    {
        _instance.winSheep = true;
        _instance.CheckIfWin();
    }

    public static void WinWolf()
    {
        _instance.winWolf = true;
        _instance.CheckIfWin();
    }

    public static void PlaySheep()
    {
        _instance.winSheep = false;
    }

    public static void PlayWolf()
    {
        _instance.winWolf = false;
    }

    float timeThenWins = 0;
    bool winTriggered;
    public void CheckIfWin()
    {
        if (winSheep && winWolf)
        {
            // Debug.Log("Wait for win");
            // timeThenWins = Time.time;
            State = GameState.Finished;
        }
    }

    public static void StartGame()
    {
        _instance.menuScreen.SetActive(false);
        State = GameState.Playing;
    }

    void Update()
    {
        // if (Time.time - timeThenWins > 5 && timeThenWins != 0 && !winTriggered)
        // {
        //     winTriggered = true;
        //     State = GameState.Win;
        // }
    }

}
