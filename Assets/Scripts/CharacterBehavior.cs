using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum ItemType { Fang, Wool }

public class CharacterBehavior : MonoBehaviour
{
    float HEALDAMAGE_AMOUNT = 1;
    float TOTAL_LIFE = 10;
    [SerializeField] ItemType itemMustCollect = ItemType.Fang;
    [SerializeField] KeyCode upBtn;
    [SerializeField] KeyCode downBtn;
    [SerializeField] float speed;
    [SerializeField] Image heartImg;

    float energy = 5; //MAX 100 - MIN 0

    private void OnTriggerEnter(Collider other)
    {
        if (GameManager.State != GameState.Playing) return;

        if (other.tag == ItemTypeStr())
        {
            if (energy < 100)
            {
                energy += HEALDAMAGE_AMOUNT;
                Debug.Log($"{name}: {energy}");
            }
            else
            {
                if (itemMustCollect == ItemType.Fang) GameManager.WinSheep();
                else GameManager.WinWolf();
            }
        }
        else if (other.tag == OtherTypeStr())
        {
            if (energy > 0)
            {
                if (itemMustCollect == ItemType.Fang) GameManager.PlaySheep();
                else GameManager.PlayWolf();
                energy -= HEALDAMAGE_AMOUNT;
                Debug.Log($"{name}: {energy}");
            }
            else
            {
                GameManager.State = GameState.Lose;
            }
        }
        else return;
        heartImg.fillAmount = energy / TOTAL_LIFE;
        Destroy(other.gameObject);
    }

    string ItemTypeStr()
    {
        switch (itemMustCollect)
        {
            case ItemType.Fang: return "Fang";
            case ItemType.Wool: return "Wool";
        }
        return "";
    }

    string OtherTypeStr()
    {
        switch (itemMustCollect)
        {
            case ItemType.Fang: return "Wool";
            case ItemType.Wool: return "Fang";
        }
        return "";
    }

    void Update()
    {
        if (GameManager.State != GameState.Playing) return;

        if (Input.GetKey(upBtn)) transform.position += Vector3.up * speed;
        if (Input.GetKey(downBtn)) transform.position += Vector3.down * speed;
    }
}
