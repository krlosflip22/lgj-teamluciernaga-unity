using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChunkSpawner : MonoBehaviour
{
    [SerializeField] Transform spawnPosition;
    [SerializeField] Transform chunkParent;
    [SerializeField] GameObject chunkPrefab;

    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "chunkCollider")
        {
            GameObject go = Instantiate(chunkPrefab, spawnPosition.position, spawnPosition.rotation);
            go.transform.parent = chunkParent;
            Destroy(other.transform.parent.gameObject);
        }
    }
}
