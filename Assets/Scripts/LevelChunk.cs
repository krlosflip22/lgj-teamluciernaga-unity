using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelChunk : MonoBehaviour
{
    [SerializeField] Transform[] spawnPoints;

    [SerializeField] GameObject[] items;

    void Awake()
    {
        foreach (Transform p in spawnPoints)
        {
            GameObject go = Instantiate(items[Random.Range(0, items.Length)], p.position + Vector3.up * Random.Range(0f, 3.8f), p.rotation);
            go.transform.parent = transform;
        }
    }
}
