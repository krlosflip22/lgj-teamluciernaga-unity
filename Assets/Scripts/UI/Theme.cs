using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace teamluciernaga.lgj2022.ui
{
    [CreateAssetMenu(fileName = "theme", menuName = "ScriptableObjects/UITheme")]
    public class Theme : ScriptableObject
    {
        public Color panelBackground;
        public Color focusedOption;
        public Color unfocusedOption;
        public Color textForeground;
    }
}
