using TMPro;
using UnityEngine;

namespace teamluciernaga.lgj2022.ui
{
    public class Menu : MonoBehaviour
    {
        private int currentOptionIndex;
        private MenuOption[] options;

        private MenuOption CurrentOption { get {  return options[currentOptionIndex]; } }

        public MenuOptionCursor LeftCursor;
        public MenuOptionCursor RightCursor;

        // Start is called before the first frame update
        void Start()
        {
            if (LeftCursor == null)
            {
                Debug.LogWarning(GetType().Name + ": No Left Cursor assigned on editor");
            }

            if (RightCursor == null)
            {
                Debug.LogWarning(GetType().Name + ": No Right Cursor assigned on editor");
            }

            options = GetComponentsInChildren<MenuOption>(true);

            if (options.Length == 0)
            {
                Debug.LogError(GetType().Name + ": No options in Menu");
            }
            else
            {
                currentOptionIndex = 0;
                CurrentOption.Select();
            }
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                PreviousOption();
            }
            else if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                NextOption();
            }
            else if (Input.GetKeyDown(KeyCode.Return))
            {
                OnActivateOption();
            }
        }

        private void SelectCurrentOption()
        {
            CurrentOption.Select();
            MoveCursors();
        }

        private void MoveCursors()
        {
            if (LeftCursor)
            {
                LeftCursor.MoveTo(CurrentOption.Left, CurrentOption.YPosition);
            }

            if (RightCursor)
            {
                RightCursor.MoveTo(CurrentOption.Right, CurrentOption.YPosition);
            }
        }

        public void UpdateCursorsPosition()
        {
            if (LeftCursor)
            {
                LeftCursor.UpdatePosition(CurrentOption.Left, CurrentOption.YPosition);
            }

            if (RightCursor)
            {
                RightCursor.UpdatePosition(CurrentOption.Right, CurrentOption.YPosition);
            }
        }

        public void NextOption()
        {
            SelectOption(currentOptionIndex + 1);
        }

        public void PreviousOption()
        {
            SelectOption(currentOptionIndex - 1);
        }

        public void SelectOption(int index)
        {
            CurrentOption.Deselect();
            currentOptionIndex = index;

            if (currentOptionIndex >= options.Length)
            {
                currentOptionIndex = 0;
            }

            if (currentOptionIndex < 0)
            {
                currentOptionIndex = options.Length - 1;
            }

            SelectCurrentOption();
        }

        public void OnActivateOption()
        {
            CurrentOption.Activate();
        }
    }
}
