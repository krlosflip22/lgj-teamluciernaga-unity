using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace teamluciernaga.lgj2022.ui.CloseDialog
{
    public class RestartOption : MenuOption
    {
        public Dialog parentDialog;

        public override void Activate()
        {
            // Add restart logic or call here
            parentDialog.Close();
            SceneManager.LoadScene("Main");
        }
    }
}
