using UnityEngine;

namespace teamluciernaga.lgj2022.ui
{
    public class Dialog : MonoBehaviour
    {
        [SerializeField] bool isPause = false;

        const float transitionTime = 0.5f;

        private float currentTime = 0f;
        private float targetScale;

        private GameObject panel;
        private Menu menu;

        public int defaultOptionIndex = 0;

        void Start()
        {
            menu = GetComponentInChildren<Menu>(true);
            panel = transform.GetChild(0).gameObject;
            targetScale = transform.localScale.x;
        }

        void Update()
        {
            if (transform.localScale.x != targetScale && currentTime < transitionTime)
            {
                currentTime += Time.deltaTime;
                var scale = Mathf.Lerp(transform.localScale.x, targetScale, currentTime / transitionTime);
                transform.localScale = new Vector3(scale, scale, 1f);
                menu.UpdateCursorsPosition();

                if (scale <= 0f)
                {
                    menu.SelectOption(defaultOptionIndex);
                    panel.SetActive(false);
                }
            }

            if (Input.GetKeyDown(KeyCode.Escape) && isPause)
            {
                Open();
            }
        }

        public void Open()
        {
            if (!panel.activeSelf && GameManager.State == GameState.Playing)
            {
                currentTime = 0f;
                targetScale = 1f;
                panel.SetActive(true);
                GameManager.State = GameState.Pause;
            }
        }

        public void Close()
        {
            if (panel.activeSelf)
            {
                currentTime = 0f;
                targetScale = 0f;
                GameManager.State = GameState.Playing;
            }
        }
    }
}
