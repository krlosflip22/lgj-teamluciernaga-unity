using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace teamluciernaga.lgj2022.ui.CloseDialog
{
    public class CancelOption : MenuOption
    {
        public Dialog parentDialog;

        public override void Activate()
        {
            parentDialog.Close();
        }
    }
}
