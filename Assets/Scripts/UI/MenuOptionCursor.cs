using TMPro;
using UnityEngine;

namespace teamluciernaga.lgj2022.ui
{
    public class MenuOptionCursor : MonoBehaviour
    {
        const float transitionTime = 0.5f;

        private float currentTime = 1f;
        private TMP_Text icon;
        private Vector3 targetPosition;

        void Awake()
        {
            icon = GetComponent<TMP_Text>();
            targetPosition = icon.rectTransform.position;
        }

        // Update is called once per frame
        void Update()
        {
            if (icon.rectTransform.position != targetPosition && currentTime < transitionTime)
            {
                currentTime += Time.deltaTime;
                icon.rectTransform.position = Vector3.Lerp(icon.rectTransform.position, targetPosition, currentTime / transitionTime);
            }
        }

        public void MoveTo(float x, float y)
        {
            targetPosition.x = x;
            targetPosition.y = y;
            currentTime = 0f;
        }

        public void UpdatePosition(float x, float y)
        {
            icon.rectTransform.position = new Vector3(x, y, icon.rectTransform.position.z);
        }
    }
}
