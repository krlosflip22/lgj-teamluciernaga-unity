using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace teamluciernaga.lgj2022.ui.TitleMenu
{

    public class StartOption : MenuOption
    {
        public override void Activate()
        {
            Debug.Log("Game Start!");
            GameManager.StartGame();
        }
    }
}
