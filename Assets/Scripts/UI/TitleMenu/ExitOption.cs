using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace teamluciernaga.lgj2022.ui.TitleMenu
{
    public class ExitOption : MenuOption
    {
        public override void Activate()
        {
            Application.Quit();
        }
    }
}
