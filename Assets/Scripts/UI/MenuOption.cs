using TMPro;
using UnityEngine;

namespace teamluciernaga.lgj2022.ui
{
    public abstract class MenuOption : MonoBehaviour
    {
        const float transitionTime = 0.5f;

        private float currentTime = 0f;
        private Color targetColor;
        private TMP_Text text;

        public float Left
        {
            get
            {
                return text ? text.rectTransform.position.x - text.rectTransform.rect.width / 2 - 10: 0;
            }
        }

        public float Right
        {
            get
            {
                return text ? text.rectTransform.position.x + text.rectTransform.rect.width / 2 + 10 : 0;
            }
        }

        public float YPosition
        {
            get
            {
                return text ? text.rectTransform.position.y : 0;
            }
        }

        void Awake()
        {
            text = GetComponent<TMP_Text>();
            text.color = ThemeManager.UnfocusedOption;
            targetColor = ThemeManager.UnfocusedOption;
        }

        void Update()
        {
            if (text.color != targetColor && currentTime < transitionTime)
            {
                currentTime += Time.unscaledDeltaTime;
                text.color = Color.Lerp(text.color, targetColor, currentTime / transitionTime);
            }
        }

        public void Select()
        {
            targetColor = ThemeManager.FocusedOption;
            currentTime = 0f;
        }

        public void Deselect()
        {
            targetColor = ThemeManager.UnfocusedOption;
            currentTime = 0f;
        }

        public abstract void Activate();
    }
}
