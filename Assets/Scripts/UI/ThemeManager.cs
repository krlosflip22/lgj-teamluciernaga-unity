using UnityEngine;

namespace teamluciernaga.lgj2022.ui
{
    public class ThemeManager : MonoBehaviour
    {
        public static Color PanelBackground { get; private set; }
        public static Color FocusedOption { get; private set; }
        public static Color UnfocusedOption { get; private set; }
        public static Color TextForeground { get; private set; }

        public Theme currentTheme;

        void Awake()
        {
            PanelBackground = currentTheme.panelBackground;
            FocusedOption = currentTheme.focusedOption;
            UnfocusedOption = currentTheme.unfocusedOption;
            TextForeground = currentTheme.textForeground;
        }
    }
}
