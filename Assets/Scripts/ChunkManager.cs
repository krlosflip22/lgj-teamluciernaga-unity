using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChunkManager : MonoBehaviour
{
    [SerializeField] float speed;
    void FixedUpdate()
    {
        if (GameManager.State != GameState.Playing) return;

        transform.position += speed * Vector3.left;
        speed += 0.00005f;
    }
}
